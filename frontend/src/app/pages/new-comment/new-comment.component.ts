import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { createCommentRequest } from '../../store/comments.actions';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-new-comment',
  templateUrl: './new-comment.component.html',
  styleUrls: ['./new-comment.component.sass']
})
export class NewCommentComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  user: Observable<User | null>;
  newUser!: User | null;
  loading: Observable<boolean>;
  error: Observable<string | null>;

  constructor(private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
    this.loading = store.select(state => state.comments.createLoading);
    this.error = store.select(state => state.comments.createError);
  }

  ngOnInit(): void {
    this.user.subscribe(user => {
      this.newUser = user;
    })
  }

  onSubmit() {
    const comment = {description: this.form.value.description,
      post: <string>this.newUser?._id, user: <string>this.newUser?._id, token: <string>this.newUser?.token};
    this.store.dispatch(createCommentRequest({commentData: comment}));
  }
}
