import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiPostData, Post } from '../../../models/post.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { ActivatedRoute } from '@angular/router';
import { fetchPostRequest } from '../../../store/posts.actions';
import { fetchCommentsRequest } from '../../../store/comments.actions';
import { ApiCommentData, Comment } from '../../../models/comment.model';
import { User } from '../../../models/user.model';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit {
  user: Observable<User | null>;
  post: Observable<Post | null>;
  comments: Observable<Comment[]>
  loading: Observable<boolean>;
  error: Observable<null | string>;
  newPost!: ApiPostData;
  newComment!: ApiCommentData;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.user = store.select(state => state.users.user);
    this.comments = store.select(state => state.comments.comments)
    this.post = store.select(state => state.posts.post);
    this.loading = store.select(state => state.posts.fetchLoading);
    this.error = store.select(state => state.posts.fetchError);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchPostRequest({id: params['id']}));
      this.store.dispatch(fetchCommentsRequest({id: params['id']}));
    });

    this.post.subscribe(post => {
      this.newPost = <ApiPostData>post;
    });
  }
}
