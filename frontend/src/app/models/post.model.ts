export class Post {
  constructor(
    public _id: string,
    public title: string,
    public description: string,
    public image: string,
    public datetime: string,
    public user:{
      _id:string,
      name:string
    }
  ) {}
}


export interface PostData {
  [key: string]: any;
  _id: string,
  title: string,
  description: string,
  image: File | null,
  datetime: string,
  user:{
    _id:string,
    name:string
  }
}

export interface ApiPostData {
  _id: string,
  title: string,
  description: string,
  image: string,
  datetime: string,
  user:{
    _id:string,
    name:string
  }
}
