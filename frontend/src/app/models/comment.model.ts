export class Comment {
  constructor(
    public _id: string,
    public description: string,
    public user: string,
    public post: string,
  ) {}
}

export interface CommentData {
  description: string,
  user: string,
  post: string
}

export interface ApiCommentData{
  _id: string,
  description: string,
  user: string,
  post: string
}
