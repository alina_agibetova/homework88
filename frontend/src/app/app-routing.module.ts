import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { NewPostComponent } from './pages/new-post/new-post.component';
import { PostsComponent } from './pages/posts/posts.component';
import { PostComponent } from './pages/posts/post/post.component';
import { NewCommentComponent } from './pages/new-comment/new-comment.component';

const routes: Routes = [
  {path: '', component: PostsComponent},
  {path: 'posts/:id', component: PostComponent},
  {path: 'comment', component: NewCommentComponent},
  {path: 'new', component: NewPostComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
