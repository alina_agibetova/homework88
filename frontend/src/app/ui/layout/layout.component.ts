import { Component, OnInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { RegisterUserData, User } from '../../models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { logoutUserRequest, registerUserRequest } from '../../store/users.actions';
import { ActivatedRoute } from '@angular/router';
import { ApiPostData } from '../../models/post.model';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.sass']
})
export class LayoutComponent implements OnInit{
  user: Observable<null | User>;
  newUser!: User | null;

  constructor(private breakpointObserver: BreakpointObserver, private store: Store<AppState>, private route: ActivatedRoute) {
    this.user = store.select(state => state.users.user);
  }

  logout() {
    this.store.dispatch(logoutUserRequest())
  }

  ngOnInit(): void {
    this.user.subscribe(user => {
      this.newUser = user;
    });
  }
}
