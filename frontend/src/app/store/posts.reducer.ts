import { PostState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createPostRequest,
  createPostsFailure,
  createPostSuccess,
  fetchPostsFailure,
  fetchPostsRequest,
  fetchPostsSuccess
} from './posts.actions';
import { fetchPostFailure, fetchPostRequest, fetchPostSuccess } from './posts.actions';

const initialState: PostState = {
  posts: [],
  post: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null
};


export const postsReducer = createReducer(
  initialState,
  on(fetchPostsRequest, state => ({...state, fetchLoading: true})),
  on(fetchPostsSuccess, (state, {posts}) => ({...state, fetchLoading: false, posts})),
  on(fetchPostsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),


  on(fetchPostRequest, state => ({...state, fetchLoading: true, fetchError: null})),
  on(fetchPostSuccess, (state, {post}) => ({...state, fetchLoading: false, post})),
  on(fetchPostFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createPostRequest, state => ({...state, createLoading: true})),

  on(createPostSuccess, state => ({...state, createLoading: false})),

  on(createPostsFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error
  }))
);




