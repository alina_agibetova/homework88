import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createPostRequest,
  createPostsFailure,
  createPostSuccess, fetchPostFailure, fetchPostRequest,
  fetchPostsFailure,
  fetchPostsRequest,
  fetchPostsSuccess, fetchPostSuccess
} from './posts.actions';
import { PostsService } from '../services/posts.service';
import { Router } from '@angular/router';
import { catchError, map, mergeMap, of, tap } from 'rxjs';

@Injectable()
export class PostsEffects {
  fetchPosts = createEffect(() => this.actions.pipe(
    ofType(fetchPostsRequest),
    mergeMap(() => this.postsService.getPosts().pipe(
      map(posts => fetchPostsSuccess({posts})),
      catchError(() => of(fetchPostsFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchPostById = createEffect(() => this.actions.pipe(
    ofType(fetchPostRequest),
    mergeMap(id => this.postsService.getPostId(id.id).pipe(
      map(post => fetchPostSuccess({post})),
      catchError(() => of(fetchPostFailure({error: 'Something went wrong'})))
    ))
  ));

  createPost = createEffect(() => this.actions.pipe(
    ofType(createPostRequest),
    mergeMap(({postData}) => this.postsService.createPost(postData).pipe(
      map(() => createPostSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createPostsFailure({error: 'Wrong data'})))
    ))
  ))

  constructor(
    private actions: Actions,
    private postsService: PostsService,
    private router: Router
  ) {}
}
