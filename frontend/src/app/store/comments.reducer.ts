import { CommentState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createCommentFailure,
  createCommentRequest,
  createCommentSuccess,
  fetchCommentsFailure,
  fetchCommentsRequest,
  fetchCommentsSuccess
} from './comments.actions';

const initialState: CommentState = {
  comments:[],
  createLoading: false,
  createError: null,
  fetchLoading: false,
  fetchError: null,
};

export const commentReducer = createReducer(
  initialState,
  on(createCommentRequest, state => ({...state, createLoading: true})),

  on(createCommentSuccess, state => ({...state, createLoading: false})),

  on(createCommentFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error
  })),

  on(fetchCommentsRequest, state => ({...state, fetchLoading: true})),
  on(fetchCommentsSuccess, (state, {comments}) => ({...state, fetchLoading: false, comments})),
  on(fetchCommentsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),
);
