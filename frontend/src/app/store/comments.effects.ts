import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { PostsService } from '../services/posts.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {
  createCommentFailure,
  createCommentRequest,
  createCommentSuccess,
  fetchCommentsFailure,
  fetchCommentsRequest,
  fetchCommentsSuccess
} from './comments.actions';

@Injectable()
export class CommentsEffects{

createComment = createEffect(() => this.actions.pipe(
  ofType(createCommentRequest),
  mergeMap(({commentData}) => this.postsService.createComment(commentData).pipe(
    map(() => createCommentSuccess()),
    tap(() => this.router.navigate(['.'], {relativeTo: this.route})),
    catchError(() => of(createCommentFailure({error: 'Wrong data'})))
  ))
))

  fetchCommentById = createEffect(() => this.actions.pipe(
    ofType(fetchCommentsRequest),
    mergeMap(id => this.postsService.getCommentById(id.id).pipe(
      map(comments => fetchCommentsSuccess({comments})),
      catchError(() => of(fetchCommentsFailure({error: 'Something went wrong'})))
    ))
  ));

constructor(
  private actions: Actions,
  private postsService: PostsService,
  private router: Router,
  private route: ActivatedRoute
) {}
}
