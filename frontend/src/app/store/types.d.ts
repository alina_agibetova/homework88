import { LoginError, RegisterError, User } from '../models/user.model';
import { Post } from '../models/post.model';
import { Comment } from '../models/comment.model';

export type PostState = {
  posts: Post[],
  post: Post | null,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string
};

export type CommentState = {
  comments: Comment[],
  createLoading: boolean,
  createError: null | string,
  fetchLoading: boolean,
  fetchError: null | string,
}


export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError
}

export type AppState = {
  posts: PostState,
  users: UsersState,
  comments: CommentState,
}
