import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginUserData, RegisterUserData, User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  registerUser(userData: RegisterUserData) {
    return this.http.post<User>('http://localhost:8000/users', userData);
  }

  login(userData: LoginUserData){
    return this.http.post<User>('http://localhost:8000/users/sessions', userData);
  }

  logout(token: string){
    return this.http.delete('http://localhost:8000/users/sessions', {
      headers: new HttpHeaders({'Authorization': token})
    });
  }
}
