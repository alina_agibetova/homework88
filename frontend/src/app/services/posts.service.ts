import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';
import { ApiPostData, Post, PostData } from '../models/post.model';
import { ApiCommentData, CommentData } from '../models/comment.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) { }

  getPosts(){
    return this.http.get<ApiPostData[]>('http://localhost:8000/posts').pipe(
      map(response => {
        return response.map(postData => {
          return new Post(
            postData._id,
            postData.title,
            postData.description,
            postData.image,
            postData.datetime,
            postData.user,
          );
        });
      })
    )
  }

  getPostId(id: string){
    return this.http.get<ApiPostData>(`http://localhost:8000/posts/${id}`).pipe(
      map(result => {
        return result;
      })
    );
  }

  getCommentById(id: string){
    return this.http.get<ApiCommentData[]>(`http://localhost:8000/comments?post=${id}`).pipe(
      map(result => {
        console.log(result)
        return result
      })
    );
  }

  createPost(postData: PostData){
    const formData = new FormData();

    Object.keys(postData).forEach(key => {
      if (postData[key] !== null){
        formData.append(key, postData[key]);
      }
    });

    return this.http.post('http://localhost:8000/posts', formData);
  }


  createComment(commentData: CommentData){
    console.log(commentData);
    return this.http.post<CommentData>('http://localhost:8000/comments', commentData);
  }
}

