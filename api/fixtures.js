const mongoose = require('mongoose');
const config = require('./config');
const Post = require('./models/Post');
const Comment = require('./models/Comment');
const User = require('./models/User')

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections){
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [Alina] = await User.create({
    email: 'test@test.com',
    password: '123',
    name: 'Alina',
  });

  const [bey] = await Post.create({
    user: Alina,
    title: 'About singer',
    description: 'some',
    image: 'beyonce.jpeg',
    datetime: new Date().toISOString(),
  });

  await Comment.create({
    user: Alina,
    post: bey,
    description: 'about beyonce',
  })

  await mongoose.connection.close();
}

run().catch(e => console.error(e))