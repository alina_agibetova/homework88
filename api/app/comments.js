const express = require('express');
const Comment = require('../models/Comment');
const mongoose = require('mongoose');

const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    if (req.query.post){
      const comment = await Comment.find({post: {_id: req.query.post}});
      return res.send(comment)
    }

  } catch (e){
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    return next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const commentData = {
      description: req.body.description,
      user: req.body.user,
      post: req.body.post,
    };

    const comment = new Comment(commentData)
    await comment.save();
    return res.send(comment);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    return next(e);
  }
});

module.exports = router;