const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const {nanoid} = require('nanoid');
const mongoose = require('mongoose');
const Post = require('../models/Post');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },

  filename: (req, file, cb) => {
    cb(null, nanoid(), path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const query = {};
    const sort = {};

    if (req.query.orderBy === 'datetime' && req.query.direction === 'desc'){
      sort._id = -1;
    }

    const posts = await Post.find(query).sort(sort).populate('user', 'name');
    return res.send(posts);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    return next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try{
    const postId = await Post.findOne({_id: req.params.id}).populate('user', 'name');

    return res.send(postId);

  } catch (e){
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    return next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try{
    if (!req.body.title){
      return res.status(400).send({message: 'Title are required'})
    }

    const postData = {
      user: req.body.user,
      title: req.body.title,
      description: req.body.description,
      image: null,
      datetime: new Date().toISOString(),
    };

    if (req.file){
      postData.image = req.file.filename;
    }
    const post = new Post(postData);
    await post.save();
    return res.send({message: 'Created new post', id: post._id});
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    return next(e);
  }
});

module.exports = router;