const mongoose = require('mongoose');
const Schema = mongoose.Schema

const PostSchema = new Schema({
  title: {
    type: String,
    required: true,
    validate: {
      validator: async function (value) {
        if (!this.isModified('title')) return true;
        const post = await Post.findOne({title: value});
        return !post;
      },
      message: 'This user is already registered'
    },
  },
  description: {
    type: String,
  },
  image: String,
  datetime: String,
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

const Post = mongoose.model('Post', PostSchema);
module.exports = Post;